# Game Controller KCM

Used to test game controllers and joysticks for [Plasma](https://kde.org/plasma-desktop/).

When a controller or joystick is connected (either via direct wired connections or bluetooth), it will appear in the KCM. It's possible to preview a visual representation of the controller, or view a more "advanced view" which displays the raw values.

## Support

If you have an issue with the Game Controller KCM, please [open a support thread on KDE Discuss](https://discuss.kde.org/c/help/6). Alternatively, you ask in the [Plasma Matrix room](https://go.kde.org/matrix/#/#plasma:kde.org). See [Matrix](https://community.kde.org/Matrix) for more details.

## Building

The easiest way to make changes and test the Game Controller KCM during development is to [build it with kdesrc-build](https://community.kde.org/Get_Involved/development/Build_software_with_kdesrc-build).

## Contributing

Please refer to the [contributing document](/CONTRIBUTING.md) for everything you need to know to get started contributing to the Game Controllers KCM.

## License

This project is licensed under the GNU General Public License 2. Other code and assets may be licensed differently, see the [REUSE metadata](https://reuse.software/).
