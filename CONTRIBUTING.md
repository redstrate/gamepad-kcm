# Contributing

Like other projects in the KDE ecosystem, contributions are welcome from all. This repository is managed in [KDE Invent](https://invent.kde.org/redstrate/gamepad-kcm), our GitLab instance.

If this is for your first code contribution, see the [GitLab wiki page](https://community.kde.org/Infrastructure/GitLab) for a tutorial on how to send a merge request.

## Installation

To ensure the KCM shows up in systemsettings, it must be installed first. If you use kdesrc-build, this is done automatically.

```shell
$ ninja install
```

## Chatting

If you get stuck or need help with anything at all, head over to the [KDE New Contributors room](https://go.kde.org/matrix/#/#kde-welcome:kde.org) on Matrix. For questions about the Game Controller KCM, please ask in the [Plasma room](https://go.kde.org/matrix/#/#plasma:kde.org). See [Matrix](https://community.kde.org/Matrix) for more details.

## What Needs Doing

Please see the [Invent Issues page](https://invent.kde.org/redstrate/gamepad-kcm/-/issues) as the KCM is still under development.