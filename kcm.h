/*
    SPDX-FileCopyrightText: 2023 Joshua Goins <josh@redstrate.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#pragma once

#include <KQuickManagedConfigModule>

class KCMGamePad : public KQuickManagedConfigModule
{
    Q_OBJECT

public:
    KCMGamePad(QObject *parent, const KPluginMetaData &metaData);
    ~KCMGamePad() override;
};
