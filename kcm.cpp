/*
    SPDX-FileCopyrightText: 2023 Joshua Goins <josh@redstrate.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "kcm.h"

#include <KPluginFactory>
#include <KSvg/ImageSet>
#include <SDL.h>

#include "axesmodel.h"
#include "buttonmodel.h"
#include "devicemodel.h"
#include "devicetypemodel.h"
#include "gamepad.h"
#include "gamepadbutton.h"

K_PLUGIN_CLASS_WITH_JSON(KCMGamePad, "kcm_gamepad.json")

KCMGamePad::KCMGamePad(QObject *parent, const KPluginMetaData &metaData)
    : KQuickManagedConfigModule(parent, metaData)
{
    SDL_Init(SDL_INIT_GAMECONTROLLER);

    setButtons(Help);

    constexpr const char *uri{"org.kde.plasma.gamepad.kcm"};

    KSvg::ImageSet *imageSet = new KSvg::ImageSet(this);
    imageSet->setBasePath(QStringLiteral(":/kcm/kcm_gamepad"));
    imageSet->setImageSetName(QStringLiteral("artwork"));

    qmlRegisterType<DeviceModel>(uri, 1, 0, "DeviceModel");
    qmlRegisterType<DeviceTypeModel>(uri, 1, 0, "DeviceTypeModel");
    qmlRegisterUncreatableType<Gamepad>(uri, 1, 0, "Gamepad", QStringLiteral("Use DeviceModel to grab Gamepads"));
    qmlRegisterUncreatableType<GamepadButton>(uri, 1, 0, "GamepadButton", QStringLiteral("Access via Gamepad"));
    qmlRegisterUncreatableType<GamepadStick>(uri, 1, 0, "GamepadStick", QStringLiteral("Access via Gamepad"));
    qmlRegisterUncreatableType<GamepadTrigger>(uri, 1, 0, "GamepadTrigger", QStringLiteral("Access via Gamepad"));
    qmlRegisterType<AxesModel>(uri, 1, 0, "AxesModel");
    qmlRegisterType<ButtonModel>(uri, 1, 0, "ButtonModel");
}

KCMGamePad::~KCMGamePad()
{
    SDL_Quit();
}

#include "kcm.moc"
