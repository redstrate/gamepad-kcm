/*
    SPDX-FileCopyrightText: 2023 Joshua Goins <josh@redstrate.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick
import QtQuick.Window
import QtQuick.Layouts
import QtQuick.Layouts as Layouts
import QtQuick.Shapes
import QtQuick.Controls as QQC2

import org.kde.kcmutils as KCM
import org.kde.kirigami as Kirigami

import org.kde.plasma.gamepad.kcm

KCM.SimpleKCM {
    id: root

    title: i18nc("@title", "Controller Information")

    property var device

    ColumnLayout {
        anchors.fill: parent

        spacing: Kirigami.Units.largeSpacing

        RowLayout {
            spacing: Kirigami.Units.largeSpacing

            Layout.fillWidth: true

            QQC2.Label {
                text: i18nc("@label:textbox", "Device:")
            }

            QQC2.TextField {
                text: i18nc("Device path", "%1 (%2)", device.name, device.path)
                readOnly: true

                Layout.fillWidth: true
            }
        }

        RowLayout {
            spacing: Kirigami.Units.largeSpacing

            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout {
                spacing: Kirigami.Units.largeSpacing

                Layout.alignment: Qt.AlignTop

                QQC2.Label {
                    text: i18nc("@label Visual representation of an axis position", "Position:")
                }

                PositionWidget {
                    id: posWidget

                    device: root.device
                }
            }

            ColumnLayout {
                spacing: Kirigami.Units.largeSpacing

                Layout.fillWidth: true
                Layout.fillHeight: true

                QQC2.Label {
                    text: i18nc("@label Gamepad buttons", "Buttons:")
                }

                Table {
                    model: ButtonModel {
                        device: root.device
                    }

                    textRole: "buttonState"

                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }
            }

            ColumnLayout {
                spacing: Kirigami.Units.largeSpacing

                Layout.fillWidth: true
                Layout.fillHeight: true

                QQC2.Label {
                    text: i18nc("@label Gamepad axes (sticks)", "Axes:")
                }

                Table {
                    model: AxesModel {
                        device: root.device
                    }

                    textRole: "axisValue"

                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }
            }
        }
    }
}
