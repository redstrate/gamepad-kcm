/*
    SPDX-FileCopyrightText: 2023 Joshua Goins <josh@redstrate.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "devicemodel.h"

#include <QTimer>

#include <SDL2/SDL.h>
#include <SDL2/SDL_joystick.h>

#include "gamepad.h"

DeviceModel::DeviceModel()
{
    // TODO: temporary event loop
    auto timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &DeviceModel::poll);
    timer->start(1);
}

Gamepad *DeviceModel::device(int index) const
{
    if (index < 0 || index > m_devices.count())
        return nullptr;

    const int sdlIndex = m_devices.keys().at(index);
    return m_devices.value(sdlIndex);
}

int DeviceModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_devices.count();
}

QVariant DeviceModel::data(const QModelIndex &index, int role) const
{
    if (!checkIndex(index)) {
        return {};
    }

    const int sdlIndex = m_devices.keys().at(index.row());
    switch (role) {
    case CustomRoles::NameRole:
        return m_devices.value(sdlIndex)->name();
    case CustomRoles::DeviceRole:
        return QVariant::fromValue(m_devices.value(sdlIndex));
    case CustomRoles::ConnectionType:
        return QVariant::fromValue(m_devices.value(sdlIndex)->connectionType());
    default:
        return {};
    }
}

QHash<int, QByteArray> DeviceModel::roleNames() const
{
    return {{CustomRoles::NameRole, "name"}, {CustomRoles::DeviceRole, "device"}, {CustomRoles::ConnectionType, "connectionType"}};
}

void DeviceModel::poll()
{
    SDL_Event event{};
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        case SDL_CONTROLLERDEVICEADDED:
            addDevice(event.cdevice.which);
            break;
        case SDL_CONTROLLERDEVICEREMOVED:
            removeDevice(event.cdevice.which);
            break;
        case SDL_CONTROLLERBUTTONDOWN:
        case SDL_CONTROLLERBUTTONUP:
            m_devices.value(event.cbutton.which)->onButtonEvent(event.cbutton);
            break;
        case SDL_CONTROLLERAXISMOTION:
            m_devices.value(event.caxis.which)->onAxisEvent(event.caxis);
            break;
        }
    }
}

void DeviceModel::addDevice(const int deviceIndex)
{
    const auto joystick = SDL_JoystickOpen(deviceIndex);
    const auto id = SDL_JoystickInstanceID(joystick);

    if (m_devices.contains(id)) {
        qWarning() << "Got a duplicate add event, ignoring. Index: " << deviceIndex;
        return;
    }

    const auto gamepad = SDL_GameControllerOpen(deviceIndex);
    if (SDL_GameControllerTypeForIndex(deviceIndex) == SDL_CONTROLLER_TYPE_VIRTUAL) {
        qWarning() << "Skipping gamepad since it is virtual. Index: " << deviceIndex;
        return;
    }

    beginInsertRows(QModelIndex(), m_devices.count(), m_devices.count());
    m_devices.insert(id, new Gamepad(joystick, gamepad, this));
    endInsertRows();
}

void DeviceModel::removeDevice(const int deviceIndex)
{
    if (!m_devices.contains(deviceIndex)) {
        qWarning() << "Invalid device index from removal event, ignoring";
        return;
    }

    const int index = m_devices.keys().indexOf(deviceIndex);

    beginRemoveRows(QModelIndex(), index, index);
    m_devices.value(deviceIndex)->deleteLater();
    m_devices.remove(deviceIndex);
    endRemoveRows();
}
